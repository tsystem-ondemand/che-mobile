var isProd = process.env.YENV === 'production';

var projectLevels = [
    'legacy.blocks',
    'common.blocks',
    'desktop.blocks',
    'content.blocks',
    'presenter.blocks',
    'model.blocks',
    isProd ? 'production.blocks' : 'development.blocks'
];

var levels = [
    { path: 'node_modules/bem-core/common.blocks', check: false },
    { path: 'node_modules/bem-core/desktop.blocks', check: false },
    { path: 'node_modules/bem-components/common.blocks', check: false },
    { path: 'node_modules/bem-components/desktop.blocks', check: false },
    { path: 'node_modules/bem-components/design/common.blocks', check: false },
    { path: 'node_modules/bem-components/design/desktop.blocks', check: false },
    { path: 'node_modules/bem-calendar/common.blocks', check: false },
    { path: 'node_modules/bem-calendar/desktop.blocks', check: false },
    { path: 'node_modules/bem-calendar/touch.blocks', check: false },
    { path: 'node_modules/bem-calendar/touch-phone.blocks', check: false }
].concat(projectLevels);

var techs = {
    // essential
    fileProvider: require('enb/techs/file-provider'),
    fileMerge: require('enb/techs/file-merge'),

    // optimization
    borschik: require('enb-borschik/techs/borschik'),

    // css
    postcss: require('enb-postcss/techs/enb-postcss'),
    postcssPlugins: [
        require('postcss-import')(),
        require('postcss-each'),
        require('postcss-for'),
        require('postcss-simple-vars')(),
        require('postcss-calc')(),
        require('postcss-nested'),
        require('rebem-css'),
        require('postcss-url')({ url: 'inline' }),
        require('autoprefixer')(),
        require('postcss-reporter')()
    ],

    // css
    stylus: require('enb-stylus/techs/stylus'),

    // js
    browserJs: require('enb-js/techs/browser-js'),

    // bemtree
    // bemtree: require('enb-bemxjst/techs/bemtree'),

    // bemhtml
    bemhtml: require('enb-bemxjst/techs/bemhtml'),
    bemjsonToHtml: require('enb-bemxjst/techs/bemjson-to-html')
};

var enbBemTechs = require('enb-bem-techs');

// конфигурация задачи 'enb make specs'
// https://ru.bem.info/forum/1012/
var configSpecs = function (config) {
    config.includeConfig('enb-bem-specs');
    var specs = config.module('enb-bem-specs').createConfigurator('specs');

    specs.configure({
        destPath: 'test.specs',
        levels: projectLevels,
        sourceLevels: levels.concat('node_modules/bem-pr/spec.blocks'),
        jsSuffixes: ['vanilla.js', 'browser.js', 'js'],
        depsTech: enbBemTechs.deps,
        templateEngine: {
            templateTech: require('enb-bemxjst/techs/bemhtml'),
            templateOptions: { sourceSuffixes: ['bemhtml', 'bemhtml.js'] },
            htmlTech: require('enb-bemxjst/techs/bemjson-to-html'),
            htmlTechOptionNames: { bemjsonFile: 'bemjsonFile', templateFile: 'bemhtmlFile' }
        }
    });
};

module.exports = function(config) {
    var isProd = process.env.YENV === 'production';

    configSpecs(config);

    config.nodes('*.bundles/*', function(nodeConfig) {
        nodeConfig.addTechs([
            // essential
            [enbBemTechs.levels, { levels: levels }],
            [techs.fileProvider, { target: '?.bemjson.js' }],
            [enbBemTechs.bemjsonToBemdecl],
            [enbBemTechs.deps],
            [enbBemTechs.files],

            // css
            /*
            [techs.postcss, {
                target: '?.css',
                oneOfSourceSuffixes: ['post.css', 'css'],
                plugins: techs.postcssPlugins
            }],
            */

            // css
            // https://github.com/enb/enb-stylus/blob/master/api.ru.md
            [techs.stylus, {
                target: '?.css',
                sourceSuffixes: ['styl', 'css'],
                url: 'inline',
                inlineMaxSize: 100,
                autoprefixer: true,
                globals: ['../../desktop.blocks/page/globals.styl']
            }],

            // bemtree
            // [techs.bemtree, { sourceSuffixes: ['bemtree', 'bemtree.js'] }],

            // bemhtml
            [techs.bemhtml, {
                sourceSuffixes: ['bemhtml', 'bemhtml.js'],
                forceBaseTemplates: true,
                engineOptions : { elemJsInstances : true }
            }],

            // html
            [techs.bemjsonToHtml],

            // client bemhtml
            [enbBemTechs.depsByTechToBemdecl, {
                target: '?.bemhtml.bemdecl.js',
                sourceTech: 'js',
                destTech: 'bemhtml'
            }],
            [enbBemTechs.deps, {
                target: '?.bemhtml.deps.js',
                bemdeclFile: '?.bemhtml.bemdecl.js'
            }],
            [enbBemTechs.files, {
                depsFile: '?.bemhtml.deps.js',
                filesTarget: '?.bemhtml.files',
                dirsTarget: '?.bemhtml.dirs'
            }],
            [techs.bemhtml, {
                target: '?.browser.bemhtml.js',
                filesTarget: '?.bemhtml.files',
                sourceSuffixes: ['bemhtml', 'bemhtml.js'],
                engineOptions : { elemJsInstances : true },
                forceBaseTemplates: true
            }],

            // js
            [techs.browserJs, { includeYM: true }],
            [techs.fileMerge, {
                target: '?.js',
                sources: ['?.browser.js', '?.browser.bemhtml.js']
            }],

            // borschik
            [techs.borschik, {
                source: '?.js',
                target: '?.min.js',
                minify: isProd
            }],

            [techs.borschik, {
                source: '?.css',
                target: '?.min.css',
                freeze: true,
                minify: isProd
            }]
        ]);

        nodeConfig.addTargets([/* '?.bemtree.js', */ '?.html', '?.min.css', '?.min.js']);
    });
};
