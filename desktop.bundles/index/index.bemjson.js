({
    block: 'page',

    title: 'Cherehapa страхование. Туристическая страховка онлайн',
    head: [
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: 'index.min.css' }
    ],
    scripts: [{ elem: 'js', url: 'index.min.js' }],

    //дополнительные блоки на теге <body>
    mix: [
        { block: 'view-router', js: true },
        { block: 'main-menu', js: true },
        { block: 'init-models' }
    ],

    content: [
        {
            block: 'header',
            url: '#home',
            tel: 'tel:8(800)5552198'
        },
        {
            block: 'main-menu',
            elem: 'content',
            content: [
                { url: '#home', content: 'В начало' },
                { url: '#countries', content: 'Выбор стран' },
                { url: '#insurable', content: 'Что и зачем страховать?' },
                { url: '#faq', content: 'Вопросы и ответы' },
                { url: '#about', content: 'О компании' },
                { url: '#contacts', content: 'Контакты' },
            ]
        },
        {
            block: 'view',
            url: '#home',
            attrs: { role: 'form' },
            content: [
                {
                    block: 'where',
                    content: {
                        block: 'countries-selected',
                        mods: { prop: 'country' }
                    }
                },
                {
                    block: 'view-button',
                    url: '#countries',
                    content: {
                        block: 'is-empty',
                        mods: { prop: 'country' },
                        content: [
                            { elem: 'yes', content: 'Выбрать страну' },
                            { elem: 'no', content: 'Добавить страну' }
                        ]
                    }
                },
                {
                    elem: 'twin',
                    content: [
                        {
                            mix: {block: 'view', elem: 'twin-left'},
                            block: 'input-date',
                            mods: { prop: 'dateStart' },
                            placeholder: 'Выезжаю'
                        },
                        {
                            mix: {block: 'view', elem: 'twin-right'},
                            block: 'input-date',
                            mods: { prop: 'dateEnd' },
                            placeholder: 'Возвращаюсь'
                        }
                    ]
                },
                {
                    block: 'checkbox',
                    mods: { prop: 'multipolicy' /*, type: 'switch'*/ },
                    text: 'Нужен годовой полис'
                },
                {
                    block: 'view-button',
                    url: '#travelers',
                    content: 'Данные путешественников'
                },
                { block: 'footer' }
            ]
        },
        {
            block: 'view',
            url: '#countries',
            title: 'Выберите страны поездки',
            attrs: { role: 'form' },
            content: [
                {
                    block: 'countries-selected',
                    mods: { prop: 'country' }
                },
                {
                    block: 'view-button',
                    mods: { 'history-back': true },
                    url: '#home',
                    content: 'OK'
                },
                {
                    block: 'countries-select',
                    mods: { prop: 'country' }
                }
            ]
        },
        {
            block: 'view',
            url: '#travelers',
            title: 'Данные о путешественниках',
            attrs: { role: 'form' },
            content: [
                { block: 'view-travelers-content' },
                {
                    block: 'view-button',
                    mods: { 'history-back': true },
                    url: '#home',
                    content: 'OK'
                }
            ]
        },
        {
            block: 'view',
            url: '#insurable',
            title: 'Что и зачем страховать?',
            attrs: { role: 'article' },
            theme: 'blue',
            content: { block: 'view-insurable-content' }
        },
        {
            block: 'view',
            url: '#faq',
            title: 'Вопросы и ответы страхования в Cherehapa',
            attrs: { role: 'article' },
            theme: 'blue',
            content: { block: 'view-faq-content' }
        },
        {
            block: 'view',
            url: '#about',
            title: 'О компании Cherehapa',
            attrs: { role: 'article' },
            theme: 'blue',
            content: { block: 'view-about-content' }
        },
        {
            block: 'view',
            url: '#contacts',
            title: 'Контакты Cherehapa',
            attrs: { role: 'article' },
            theme: 'blue',
            content: { block: 'view-contacts-content' }
        },
        {
            block: 'view',
            title: 'Cherehapa: ошибка 404',
            theme: 'blue',
            content: { block: 'view-404-content' }
        }

    ]
})
