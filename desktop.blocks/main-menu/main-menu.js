modules.define('main-menu', ['i-bem-dom'],
function(provide, bemDom) {
provide(bemDom.declBlock(this.name, {

    onSetMod: {
        'js': {
            'inited': function () {
                this._domEvents(bemDom.win).on('click', function () {
                    this.delMod('visible');
                });
            }
        }
    }

},
{
    lazyInit: true,

    onInit: function () {
        this._domEvents('icon').on('click', function (e) {
            this.toggleMod('visible');
            e.stopPropagation();
        });
    }
}));
}
);
