modules.define('view-router', ['view', 'i-bem-dom'],
function(provide, View, bemDom) {
provide(bemDom.declBlock(this.name, {

    views: [], //массив блоков view, c url или без (например: попапы)
    pages: {}, //блоки view с параметром url

    onSetMod: {
        'js': {
            'inited': function() {
                this.views = [];
                this.pages = {};
                this.defaultTitle = window.document.title;

                var views = this.findChildBlocks(View).toArray();
                if (!views || !views.length) {
                    throw 'требуется хотя бы один блок view';
                }
                this.views = views;

                views.forEach( function (view) {
                    var url = view.getUrl();
                    if (url) {
                        if (this.pages[url]) {
                            throw ('дублируется view c url: ' + url);
                        }

                        this.pages[url] = view;
                    }
                }.bind(this));

                this._domEvents(bemDom.win).on('hashchange', function() {
                    this.render(window.location.hash);
                }.bind(this));

                this.render(window.location.hash);
            }
        }
    },

    render: function(url) {
        var view = this.pages[url];
        if (!view) {
            /* Соглашение:
                первый view является страницей по умолчанию (с пустым url)
                последний view вызывается для обработки всех остальных неизвестных url
            */
            var i = (!url || url === '#') ? 0 : (this.views.length-1);
            view = this.views[i];

            var canonicalUrl = view.getUrl();
            if (canonicalUrl) {
                this.redirect(canonicalUrl);
                return;
            }
        }

        window.document.title = view.params.title || this.defaultTitle;

        this.views.forEach( function(otherView) {
            (otherView !== view) && otherView.delMod('visible');
        });

        view.setMod('visible');
    },

    redirect: function(url) {
        window.location.href = url;
    }

}));
}
);
