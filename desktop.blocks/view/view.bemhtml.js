block('view')(
    tag()('section'),
    mix()('clearfix'),

    js()(function() {
        var params = {};

        ['url', 'title', 'header', 'theme'].forEach( function (param) {
            if (this.ctx[param] != null) {
                params[param] = this.ctx[param];
            }
        }.bind(this));

        return params;
    })
)

block('view').elem('twin').mix()({block: 'clearfix'});
