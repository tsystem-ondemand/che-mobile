block('countries-selected').elem('item')(
    content()(function () {
        return [
            {
                elem: 'content',
                tag: 'span',
                content: this.ctx.content
            },
            {
                elem: 'control',
                tag: 'span',
                attrs: { role: 'button' }
            }
        ];
    })
);
