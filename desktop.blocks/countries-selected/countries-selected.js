modules.define('countries-selected', ['arrays', 'dom', 'BEMHTML', 'i-bem-dom', 'i-bem__internal', 'jquery'],
function(provide, Arrays, DOM, BEMHTML, bemDom, bemInternal, $) {
provide(bemDom.declBlock(this.name,
{
    onSetMod: {
        js: {
            inited: function () {
                this.items = [];
            }
        }
    },

    render: function (items) {
        if (Arrays.equal(items, this.items)) {
            return;
        }
        this.items = Arrays.clone(items);
        this._apply(items);
    },

    _apply: function (items) {
        var bemjson = this._template(items);
        this.domElem.get(0).innerHTML = BEMHTML.apply(bemjson); //bemDom.update(this.domElem, BEMHTML.apply(bemjson));
    },

    _template: function (items) {
        var blockName = this.__self._blockName;
        var elemName = 'item';
        return items.map( function (item) {
            return {
                block: blockName,
                elem: elemName,
                content: item
            };
        }.bind(this));
    },

    _onClick: function (e) {
        var selector = '.' + this.__self._blockName + bemInternal.ELEM_DELIM + 'item';
        var domElem = $(e.target).closest(selector).get(0);
        this._remove(domElem);
    },

    _remove: function (domElem) {
        var item = domElem.textContent.trim();

        var i = this.items.indexOf(item);
        if (i === -1) {
            return;
        }

        this.items.splice(i, 1);
        DOM.remove(domElem);
        this._emit('remove', item);
    }
},
{
    onInit: function () {
        this._domEvents('control').on('click', this.prototype._onClick);
    }
}
));
}
);
