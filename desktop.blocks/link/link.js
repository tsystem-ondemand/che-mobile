modules.define('link', ['i-bem-dom'], function(provide, bemDom) {

    provide(bemDom.declBlock(this.name, {})); // Объявляем базовый блок

});

// Доопределяем базовый блок с модификатором history-back
modules.define('link', function(provide, Base) { provide(Base.declMod({ modName: 'history-back' }, {

},
{
    lazyInit : true,

    onInit: function () {
        this._domEvents('control').on('pointerclick', function (e) {
            if (this.hasMod('history-back') && window.history && window.history.length) {
                window.history.back();
                e.preventDefault();
                return;
            }

            return this._onPointerClick(e);
        });

        return this.__base.apply(this, arguments);
    }
}
));

});
