modules.define('accordion-item', ['i-bem-dom'],
function(provide, bemDom) { provide(bemDom.declBlock(this.name, {
},
{
    lazyInit : true,

    onInit: function () {
        this._domEvents('title').on('click', function (e) {
            this.toggleMod('active');
        });
    }
}
))});
