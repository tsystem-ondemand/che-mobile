({ shouldDeps: [
    'countries-data',
    'arrays',
    'dom',
    'BEMHTML',
    'input',
    'softmatch',
    { block: 'functions', elem: 'debounce' }
]})
