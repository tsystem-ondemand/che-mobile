modules.define('countries-select', ['input', 'countries-data', 'arrays', 'dom', 'functions__debounce', 'softmatch', 'i-bem-dom', 'BEMHTML'],
function(provide, Input, CountriesData, Arrays, DOM, debounce, SoftMatch, bemDom, BEMHTML) { provide(bemDom.declBlock(this.name,
{
    onSetMod: {
        js: {
            inited: function () {
                this.inputTextBlock = this.findChildBlock(Input);

                this.selectedCountries = []; //уже выбранные страны иключаются из выбора (изначально ничего)
                this.visibleCountries = []; //все выбранные страны на данный момент
                this.clearFilter();

                var specialKeys = {
                    '13': function() { this.submit(this.inputTextBlock.getVal()); }.bind(this),
                    '38': function() { console.warn('prev not implemented'); },
                    '40': function() { console.warn('next not implemented'); }
                };

                var debounceSetFilter = debounce( function() {
                    this.setFilter(this.inputTextBlock.getVal());
                }.bind(this), 100);

                this._domEvents(this.inputTextBlock).on('keyup', function(e) {
                    var key = (e.keyCode || e.which);
                    if (specialKeys[key]) {
                        return specialKeys[key](e);
                    }

                    debounceSetFilter();
                }.bind(this));
            }
        }
    },

    render: function (items) {
        if (items != undefined) {
            this.selectedCountries = items;
        }

        var visibleCountries = Arrays.diff(this.filteredCountries, this.selectedCountries);
        if (Arrays.equal(visibleCountries, this.visibleCountries)) {
            return;
        }

        this.visibleCountries = visibleCountries;

        var domElem = this.findChildElem('countries-list').domElem.get(0); //необходимо найти динамически
        this.__self.replaceItems(domElem, visibleCountries);
    },

    selectCountry: function (countryName) {
        this.clearFilter();
        this._emit('select', countryName);
    },

    clearFilter: function () {
        this.inputTextBlock.setVal('');

        this.lastText = '';
        this.filteredCountries = this.__self.filterCountries('');
    },

    setFilter: function (text) {
        if (text == this.lastText) {
            return;
        }

        this.lastText = text;
        this.filteredCountries = this.__self.filterCountries(this.lastText);

        this.render();
    },

    submit: function (text) {
        this.lastText = text;
        this.filteredCountries = this.__self.filterCountries(this.lastText);

        var visibleCountries = Arrays.diff(this.filteredCountries, this.selectedCountries)
        if (visibleCountries.length === 1) {
            var countryName = visibleCountries[0];
            this.selectCountry(countryName);
            return;
        }

        this.render();
    }
},
{
    onInit: function () {
        this.initCountriesData();

        this._domEvents('country').on('click', function (e) {
            var countryName = e.target.textContent.trim();
            this.selectCountry(countryName);
        });
    },

    replaceItems: function (domElem, items) {
        var fragment = document.createDocumentFragment();

        items.forEach( function(item) {
            var itemElem = this._itemsElems[item];
            itemElem && fragment.appendChild(itemElem);
        }.bind(this));

        DOM.replaceChildren(domElem, fragment);
    },

    initCountriesData: function () {
        this._countries = []; //список названий стран
        this._itemsElems = {}; //хеш DOM элементов для каждой страны
        this._searchIndex = {}; //хеш алиасов тектового поиска
        this._queryCache = {}; //кеш операции фильтрования стран по первым двум буквам

        var bemjson = [];
        CountriesData.getAllCountriesGroups().forEach( function(country) {
            var countryName = country.name;

            this._countries.push(countryName);
            this._searchIndex[countryName] = SoftMatch.makeSoft(country.search);
            bemjson.push({
                block: 'countries-select', elem: 'country',
                elemMods: { group: country.group },
                content: countryName
            });
        }.bind(this));

        var tempElem = document.createElement('div'); //нужен именно DOM element, т.к. DOM fragment не имеет свойства innerHTML
        tempElem.innerHTML = BEMHTML.apply(bemjson);
        var nodes = Array.prototype.slice.call(tempElem.childNodes);
        this._countries.forEach(function (item, i) {
            this._itemsElems[item] = nodes[i];
        }.bind(this));
    },

    filterCountries: function (text) {
        var query = text.trim().toLowerCase();

        //затратно и малополезно искать по единственной букве
        if (query.length < 2) {
            return Arrays.clone(this._countries);
        }

        //кешируем результаты всех запросов из 2-х букв
        var softKey = SoftMatch.makeSoft(query.substr(0, 2));
        if (!this._queryCache[softKey]) {
            this._queryCache[softKey] = this._filter(softKey, this._countries);
        }

        var softQuery = SoftMatch.makeSoft(query);

        //оптимизация простейшего случая
        if (softKey == softQuery) {
            return Arrays.clone(this._queryCache[softKey]);
        }

        return this._filter(softQuery, this._queryCache[softKey]);
    },

    _filter: function (query, items) {
        return items.filter( function(item) {
            return SoftMatch.matchStrict(query, this._searchIndex[item]);
        }.bind(this));
    }

}
));
}
);
