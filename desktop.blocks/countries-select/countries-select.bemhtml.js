block('countries-select')(
    content()([
        {
            block: 'input',
            mix: [
                { block: 'countries-select', elem: 'text' },
                { block: 'input-text' }
            ]
        },
        { elem: 'countries-list' }
    ])
);
