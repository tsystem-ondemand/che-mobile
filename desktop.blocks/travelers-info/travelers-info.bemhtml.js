block('travelers-info')(
    mix()({ block: 'input-text' }),
    content() (
       [
           {
               block: 'input',
               placeholder: 'Дата рождения'
           },
           {
               block: 'input',
               placeholder: 'Имя'
           },
           {
               block: 'input',
               placeholder: 'Фамилия'
           }
       ]
    )

);
