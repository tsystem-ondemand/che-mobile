modules.define('arrays',
function (provide) {

    var Arrays = {};

    Arrays.clone = function (array) {
        return array.slice();
    };

    Arrays.findIndex = function (array, predicate, thisArg) {
        for (var i = 0, length = array.length; i < length; ++i) {
            if (predicate.call(thisArg, array[i], i, array)) {
                return i;
            }
        }
        return -1;
    };

    Arrays.find = function (array, predicate, thisArg) {
        var i = Arrays.findIndex(array, predicate, thisArg);
        return (i === -1) ? undefined : array[i];
    };

    Arrays.indexOf = function (array, value, key) {
        if (!key) {
            return array.indexOf(value);
        }

        return Arrays.findIndex(array, function(element) {
            return element[key] === value;
        });
    };

    Arrays.equal = function (a, b) {
        if (a.length !== b.length) {
            return false;
        }

        return a.every( function(value, i) {
            return value === b[i];
        });
    };

    Arrays.diff = function (a, b) {
        if (!b.length) {
            return Arrays.clone(a);
        }

        return a.filter( function(value) {
            return b.indexOf(value) === -1;
        });
    };

    Arrays.only = function (a, b) {
        if (!b.length) {
            return [];
        }

        return a.filter( function(value) {
            return b.indexOf(value) !== -1;
        });
    };

    Arrays.merge = function (a, b) {
        return [].concat(a, Arrays.diff(b, a));
    };

    Arrays.pushUnique = function (array, value) {
        return Arrays.merge(array, [value]);
    };

    Arrays.remove = function (array, value) {
        return Arrays.diff(array, [value]);
    };

    Arrays.uniqueSort = function (array) {
        var sorted = array.sort();

        return sorted.filter( function(value, i) {
            return (i === 0) || (value !== sorted[i-1]);
        });
    };

    provide(Arrays);

}
);
