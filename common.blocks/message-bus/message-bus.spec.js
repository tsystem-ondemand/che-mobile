modules.define('spec', ['message-bus', 'chai'],
function (provide, MessageBus, chai) {
    var assert = chai.assert;

    describe('message-bus', function() {

        it('должен подписаться на шину и получить отправленные сообщения', function(done) {
            var messageBus = new MessageBus();

            var totalMessages = 0;
            var subscriber = function (message) {
                totalMessages += 1;
            };

            messageBus.subscribe(subscriber);

            messageBus.send(0);
            messageBus.send(1);
            messageBus.send(2);
            messageBus.send(3);

            setTimeout(function () {
                assert.equal(totalMessages, 4, 'все сообщения');
                done();
            }, 50);

        });

    });

    provide();

});
