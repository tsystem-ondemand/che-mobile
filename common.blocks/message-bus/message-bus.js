modules.define('message-bus', function(provide) {

    var MessageBus = function() {
        this.subscribers = [];
    };

    /**
     * Отправка сообщения всем ранее зарегистированным подписчикам
     *
     * @param message object само сообщение
     **/
    MessageBus.prototype.send = function(message) {
        var args = arguments;
        this.subscribers.forEach( function(subscriber) {
            //все подписчики уведомляются параллельно
            setTimeout( function() {
                subscriber.apply(undefined, args);
            }, 0);
        });
    };

    /**
     * Регистрация подписчика
     *
     * @param subscriber function(message) функция-получатель сообщения
     * @return ключ данного подписчика, по которому можно потом отписать себя
     **/
    MessageBus.prototype.subscribe = function(subscriber) {
        return this.subscribers.push(subscriber) - 1;
    };

    /**
     * отмена регистрации подписчика
     **/
    MessageBus.prototype.unsubscribe = function(subscriberKey) {
        /**
         * заменяем функцию подписки на пустую, чтобы индексы массива не изменились
         **/
        this.subscribers[subscriberKey] = function() {};
    };

    provide(MessageBus);

});
