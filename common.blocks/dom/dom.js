modules.define('dom',
function (provide, Base) {

    var DOM = Base;

    //быстрая замена всех элементов внутри DOM-node
    DOM.replaceChildren = function (node, fragment) {
        var newNode = node.cloneNode(false);
        newNode.appendChild(fragment);
        node.parentNode.replaceChild(newNode, node);
    }

    //удаляет себя из своего родителя
    DOM.remove = function (node) {
        node.parentNode.removeChild(node);
    }

    provide(DOM);
}
);
