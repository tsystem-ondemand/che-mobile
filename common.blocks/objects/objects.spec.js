modules.define('spec', [ 'objects', 'chai'],
function (provide, Objects, chai) {
    var expect = chai.expect;
    var clone = Objects.clone;

    describe('Objects.clone', function() {
        it('should accept various simple data types', function() {
            [ null, undefined, false, true, 0, 34, 0xff, (0.1+0.2), -3e-5, '', 'Черехапа', "00" ]
                .forEach( function (data) {
                    expect(clone(data)).to.equal(data);
                });

            [ Infinity, -Infinity, NaN ]
                .forEach( function (badNumber) {
                    expect(clone(badNumber)).to.be.null;
                });
        });

        it('should clone simple objects', function() {
            [ {}, [] ]
                .forEach( function (data) {
                    expect(clone(data)).not.equal(data);
                    expect(clone(data)).deep.equal(data);
                });

            var o = {};
            var oo = clone(o);
            expect(o).deep.equal(oo);

            o['x'] = 1;
            expect(o).not.deep.equal(oo);

            var a = [];
            var aa = clone(a);
            expect(a).deep.equal(aa);

            aa.push(1);
            expect(a).not.deep.equal(aa);
        });

        it('should deep clone various objects', function() {
            [ [[]], [{}], [1, 2, 3], {a: 'b', c: 'd'}, {a: {b: {c: 1}}} ]
                .forEach( function (data) {
                    expect(clone(data)).not.equal(data);
                    expect(clone(data)).deep.equal(data);
                });

            var x = {a: 1};

            var o = {x: x};
            var oo = clone(o);
            expect(o).deep.equal(oo);

            x.a = 2;

            expect(o).not.deep.equal(oo);
        });

    });

    describe('Objects.only', function() {
        it('should work', function() {
            [
                [{}, [], {}],
                [{}, ['b', 'c'], {}],
                [{ a: 1, b: 2, c: 3}, [], {}],
                [{ a: 1, b: 2, c: 3}, ['b', 'c'], { b: 2, c: 3}],
                [{ a: 1, b: 2, c: 3}, ['d', 'c', 'b'], { c: 3, b: 2}],
                [{ a: 1, b: 2, c: 3}, ['d', 'e', 'f'], {}]
            ]
                .forEach( function (test) {
                    expect( Objects.only(test[0], test[1]) ).deep.equal(test[2]);
                });

        });
    });

    describe('Objects.flip', function() {
        it('should work', function() {
            [
                [{}, {}],
                [{ a:'b' }, { b:'a' }],
                [{ a: 'a', b: 'b'}, { a: 'a', b: 'b'}],
                [{ a: 'b', b: 'a'}, { a: 'b', b: 'a'}],
                [{ a: 1, b: 2, c: 3}, {'1':'a', '2':'b', '3':'c'}]
            ]
                .forEach( function (test) {
                    expect( Objects.flip(test[0]) ).deep.equal(test[1]);
                });

        });
    });

    provide();

});
