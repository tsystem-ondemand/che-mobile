/**
 * Глобальная очередь сообщений о событиях models во views
 */
modules.define('response-bus', ['message-bus'],
function (provide, MessageBus) {
provide(

    new MessageBus('responseBus')

);
}
);
