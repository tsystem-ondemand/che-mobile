#!/bin/sh
ssh root@staging.cherehapa.ru "mkdir -p /var/www/che-static/build/ && mkdir -p /var/www/che-static/build/mobile"
scp desktop.bundles/index/index.html root@staging.cherehapa.ru:/var/www/che-static/build/mobile/index.html
scp desktop.bundles/index/index.min.css root@staging.cherehapa.ru:/var/www/che-static/build/mobile/index.min.css
scp desktop.bundles/index/index.min.js root@staging.cherehapa.ru:/var/www/che-static/build/mobile/index.min.js
scp favicon.ico root@staging.cherehapa.ru:/var/www/che-static/build/mobile/favicon.ico
