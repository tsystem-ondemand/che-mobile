modules.define('storage-persistent', ['storage', 'localforage', 'functions__debounce' ],
function(provide, stateStorage, localforage, debounce) {

    var config = {
        name: 'cherehapa',
        storeName: 'model-storage',
        item: 'state',
        description: 'Хранилище данных приложения mobile.cherehapa.ru'
    };

    localforage.config(config);

    localforage.getItem(config.item, function(err, value) {
        /**
         * если в localforage пусто, то мы НЕ хотим выполнять request на изменение состояния модели на пустое
         * ведь кто-то другой может инициализировать модель на более полезное начальное состояние, чем пустое
         **/
        if (value) {
            stateStorage.updateState(value);
        }

        /**
         * подписываемся на выполнение сохранений состояния лишь после того,
         * как сами предварительно отправили первый request с состоянием из localforage
         * иначе может получиться, что мы затрем сохранённое в localforage до того, как успели его загрузить в модель
         **/
        setTimeout( function() {
            /**
             * эта функция будет выполняться на каждый response oт модели,
             * поэтому мы защитим её с помощью debounce() от спама localforage слишком частыми запросами
             **/
            var saveModelState = debounce( function(state) {
                localforage.setItem(config.item, state, function() {
                    console.log('localforage.save', state);
                });
            }, 1000);

            stateStorage.subscribe(saveModelState);
        }, 1000);
    });

    provide();

});
