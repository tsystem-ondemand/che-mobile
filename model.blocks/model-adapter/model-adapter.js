modules.define('model-adapter', ['storage', 'request-bus', 'response-bus', 'command', 'dates-model', 'countries-model', 'functions__debounce'],
function(provide, stateStorage, requestBus, responseBus, Command, DatesModel, CountriesModel, debounce) {

    var models = [];

    models.push(new DatesModel());
    models.push(new CountriesModel());
    models.push(new Command()); //должна быть последней


    /**
     * Эта функция выполняется в ответ на специальный пустой request.
     * Такой request должен отправить каждый новый слушатель модели,
     * т.е при старте приложения почти сразу же будет создано множество слушателей
     * и таких одинаковых запросов будет создано очень много практически одновременно,
     * а потом (скорее всего) больше вообще не будет.
     *
     * Чтобы немного разгрузить процесс начальной иниициализации,
     * мы защитим данную функцию c помощью debounсe(),
     * т.к. на все запросы одинаковые начальные запросы
     * response совершенно одинаковый и его одного вполне достаточно на всех.
     */
    var pingResponse = debounce( function() {
        stateStorage.pingState();
    }, 20);

    var sendResponse = function(state, changedKeys) {
        /**
         * собственно это и есть формат response сообщения
         **/
        var response = {
            state: state,
            changedKeys: changedKeys
        };

        models.forEach( function(model) {
            response = model.transformResponse(response);
        });

        responseBus.send(response);
    };

    var handleRequest = function(request) {
        /**
         * особая команда, которую нужно отдельно обрабатывать
         **/
        if (request.cmd === 'ping') {
            pingResponse();
            return;
        }

        var state = stateStorage.cloneState();

        /**
         * здесь мы делаем то, что должна делать бизнес-логика
         * до записи в State
         **/
        models.forEach( function(model) {
            request = model.transformRequest(request, state);
        });

        /**
         * собственно запись в State переработанного request
         **/
        stateStorage.updateState(request);
    };

    var init = function() {
        /**
         * подписываемся на события изменения State
         **/
        stateStorage.subscribe(sendResponse.bind(this));

        /**
         * подписываемся на сообщения из шины request
         **/
        requestBus.subscribe(handleRequest.bind(this));

        /**
         * Модель сообщает всем слушателям созданным раньше чем сама модель,
         * что она родилась
         **/
        pingResponse();
    };

    init();
    provide();
});
