({ shouldDeps: [
    'storage',
    'request-bus', 'response-bus',
    'command', 'dates-model', 'countries-model',
    { block: 'functions', elem: 'debounce' }
] })
