modules.define('storage', ['message-bus', 'objects'],
function(provide, MessageBus, Objects) {

    var clone = Objects.clone;

    var Storage = function() {
        this._bus = new MessageBus('storage');
        this._state = {};
    };

    Storage.prototype.cloneState = function() {
        return clone(this._state);
    };

    /**
     * Для каждого одноименного собственного свойства из object
     * создаем или полностью заменяем свойства внутреннего состояния _state
     **/
    Storage.prototype.updateState = function(object) {
        var nextState = this.cloneState();

        var changedKeys = [];
        Objects.each(object, function(value, key) {
            if (nextState[key] !== value) {
                nextState[key] = clone(value);
                changedKeys.push(key);
            }
        });

        /**
         * если оказалось, что фактических изменений не было,
         * то никаких событий мы не станем отправлять
         **/
        if (changedKeys.length) {
            this._state = nextState;
            this._bus.send(this.cloneState(), changedKeys);
        }
    };

    Storage.prototype.pingState = function() {
        this._bus.send(this.cloneState(), []);
    };

    Storage.prototype.subscribe = function(subscriber) {
        this._bus.subscribe(subscriber);
    };

    provide(new Storage);

});
