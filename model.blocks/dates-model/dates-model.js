modules.define('dates-model', ['dates', 'objects', 'arrays'],
function(provide, Dates, Objects, Arrays) {

    var minDate = function() {
        var moscowDateNow = Dates.calendarDate(-180);
        return Dates.plusDays(moscowDateNow, 1);
    };

    var maxDate = function(startDate) {
        var baseDate = (startDate != null) ? startDate : minDate();
        return Dates.plusDays(Dates.plusYear(baseDate), -1);
    };

    var DatesModel = function() {};

    DatesModel.prototype.props = ['dateStart', 'dateEnd', 'multipolicy'];

    DatesModel.prototype.transformRequest = function(originalRequest, state) {
        var request = Objects.clone(originalRequest);

        //ссылка для удобства
        var props = request.value;

        /**
         * Нормализуем даты в разных форматах из разных источников
         */
        ['dateStart', 'dateEnd'].forEach( function(key) {
            if (props[key] != null) {
                props[key] = Dates.formatDate(props[key]);
            }
        });

        var dateStart = props.dateStart || state.dateStart;
        var dateEnd = props.dateEnd || state.dateEnd;

        //исправляем, если даты начала и конца перепутаны
        if (dateStart && dateEnd && Dates.diffDays(dateEnd, dateStart) < 0) {
            props.dateStart = Dates.formatDate(dateEnd);
            props.dateEnd = Dates.formatDate(dateStart);
        }

        //исправляем устаревшую дату окончания так, чтобы длительность поездки не изменилась
        if (dateStart && Dates.diffDays(dateStart, minDate()) < 0) {
            if (dateEnd) {
                //длительность поездки
                var duration = Dates.diffDays(dateEnd, dateStart);
                props.dateEnd = Dates.plusDays(minDate(), duration);
            }
            props.dateStart = minDate();
        }

        //устанавливаем мультиполис, если интервал год или больше
        if (dateEnd && Dates.diffDays(dateEnd, maxDate(dateStart)) >= 0) {
            props.multipolicy = true;
        }

        if (props.multipolicy === true) {
            //устанавливаем отсутствующую дату начала
            if (!dateStart) {
                props.dateStart = minDate();
            }

            props.dateEnd = null;
        }

        //возможно вообще никаких дат не было в request
        return request;
    };

    DatesModel.prototype.transformResponse = function(originalResponse) {
        var props = originalResponse.state;
        var response = Objects.clone(originalResponse);

        var dateStart = props.dateStart || null;
        response.state.dateStart = {
            select: dateStart,
            min: minDate(),
            max: maxDate()
        };

        if (props.multipolicy === true) {
            response.state.dateEnd = {
                disabled: true,
                select: maxDate(dateStart),
                min: dateStart || minDate(),
                max: maxDate(dateStart)
            };
        }
        else {
            var dateEnd = props.dateEnd || null;
            response.state.dateEnd = {
                disabled: false,
                select: dateEnd,
                view: dateEnd || dateStart || minDate(),
                min: dateStart || minDate(),
                max: maxDate(dateStart)
            };
        }

        if (Arrays.only(response.changedKeys, ['dateStart', 'multipolicy']).length) {
            response.changedKeys = Arrays.merge(response.changedKeys, ['dateEnd']);
        }

        return response;
    };

    provide(DatesModel);
});
