modules.define('countries-select', ['presenter'],
function(provide, Presenter, Base) {
provide(Base.declMod({ modName: 'prop', modVal: '*' },
{
    onSetMod: {
        js: {
            inited: function() {
                this.__base.apply(this, arguments);

                this.prop = this.getMod('prop');
                this.presenter = new Presenter(this);

                this._events().on('select', function (e, item) {
                    this.presenter.sendCmd('add', this.prop, item);
                });

                this.presenter.onSetProps = function(props) {
                    var items = props[this.prop] || [];
                    this.render(items);
                }.bind(this);

                this.presenter.subscribe(this.prop);
            }
        }
    }
}
));
}
);
