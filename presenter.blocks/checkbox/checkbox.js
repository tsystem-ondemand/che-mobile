modules.define('checkbox', ['presenter'],
function(provide, Presenter, Base) {
provide(Base.declMod({ modName: 'prop', modVal: '*' }, {

    onSetMod: {
        js: {
            inited: function() {
                this.__base.apply(this, arguments);

                this.prop = this.getMod('prop');
                this.presenter = new Presenter(this);

                this._events().on({ modName: 'checked', modVal: '*' }, function() {
                    this.presenter.sendProp(this.hasMod('checked'));
                });

                this.presenter.onSetProps = function (props) {
                    var checked = props[this.prop] ? true : false;
                    this.setMod('checked', checked);
                }.bind(this);

                this.presenter.subscribe(this.prop);
            }
        }
    }

}));
}
);
