modules.define('input-date', ['presenter'],
function(provide, Presenter, Base) {
provide(Base.declMod({ modName: 'prop', modVal: '*' }, {

    onSetMod: {
        js: {
            inited: function () {
                this.__base.apply(this, arguments);

                this.prop = this.getMod('prop');
                this.presenter = new Presenter(this);

                this._events().on('select', function (e, dateable) {
                    this.presenter.sendProp(dateable);
                });

                this.presenter.onSetProps = function (props) {
                    var datepickerProps = props[this.prop];
                    this.render(datepickerProps);
                }.bind(this);

                this.presenter.subscribe(this.prop);
            }
        }
    }

}));
}
);
