modules.define('is-empty', ['i-bem-dom'],
function(provide, bemDom) { provide(bemDom.declBlock(this.name, {
    //пусто
}));
}
);

modules.define('is-empty', ['presenter', 'types'],
function(provide, Presenter, Types, Base) {
provide(Base.declMod({ modName: 'prop', modVal: '*' }, {

    onSetMod: {
        js: {
            inited: function() {
                this.__base.apply(this, arguments);

                this.prop = this.getMod('prop');
                this.presenter = new Presenter(this);

                this.presenter.onSetProps = function (props) {
                    var isEmpty = Types.isEmpty(props[this.prop]);
                    this.setMod('no', !isEmpty);
                }.bind(this);

                this.presenter.subscribe(this.prop);
            }
        }
    }

}));
}
);
