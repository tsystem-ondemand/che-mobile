modules.define('model-mock', ['request-bus', 'response-bus', 'objects'],
function (provide, requestBus, responseBus, Objects) {
    var response = {
        changedKeys: [ 'test' ],
        state: {
            test: 43
        }
    };

    requestBus.subscribe( function(request) {
        if (request.cmd == 'set') {
            response.state = Objects.clone(request.value);
        }
        responseBus.send(response);
    });

    provide();

    //имитируем долгое создание модели
    setTimeout(function() {
        responseBus.send(response);
    }, 50);
}
);

modules.define('spec', ['presenter', 'chai'],
function (provide, Presenter, chai) {
    var assert = chai.assert;

    describe('presenter', function() {
        var presenter = new Presenter();

        it('должен подписаться и получить сообщение от модели', function(done) {
            assert.equal(presenter.isInited, false);

            presenter.onSetProps = function (props) {
                assert.equal(props.test, 43);
                done();
            };
      
            presenter.subscribe('test');         
        });

        it('должен отправить новое значение и получить его от модели', function(done) {
            assert.equal(presenter.isInited, true);

            presenter.onSetProps = function (props) {
                assert.equal(props.test, 56);
                done();
            };

            presenter.sendProp(56);
        });
    });

    //асинхронно загружаем модель
    setTimeout(function() {
        modules.require(['model-mock'], function() {});
    }, 100);

    provide();
});
