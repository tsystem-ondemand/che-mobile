modules.define('presenter', ['request-bus', 'response-bus', 'objects', 'arrays'],
function(provide, requestBus, responseBus, Objects, Arrays) {
    /*
    * есть неоднозначность общеупотребимого понятия prop (свойство) модели
    * в одних контекстах это означает имя (название) свойства (key),
    * в других случаях под свойством подразумевают значение (value) свойства с данным именем
    */

    var Presenter = function(block) {
        this.block = block; //используется только для вывода отладки в консоль

        this.writeKeys = []; //имена свойств модели, которые данный презентер имеет право изменять
        this.blockKeys = []; //все имена свойств модели, за которыми наблюдает данный презентер

        this.isInited = false;
        this._subscribeKey = null;
        this.onSetProps = function() {};
    };

    Presenter.prototype._requestBus = requestBus;
    Presenter.prototype._responseBus = responseBus;

    Presenter.prototype.subscribe = function(writeKeys, readKeys) {
        this.writeKeys = [].concat(writeKeys || []);
        this.blockKeys = [].concat(this.writeKeys, readKeys || []);

        //подписываемся на получение событий из шины response об изменении свойств модели
        this._subscribeKey = this._responseBus.subscribe(this._onResponse.bind(this), this.blockKeys, this.block);

        /*
         * команда ping запрашивает начальное состояние свойств модели
         * асинхронно получив первый ответ от модели, презентер устанавливает this.isInited = true
         */
        this._requestBus.send({ cmd: 'ping', value: this.blockKeys });
    };

    Presenter.prototype.sendCmd = function(cmd, key, value) {
        if (!this.isInited) {
            //не спамим командами шину пока не уверены, что нас уже слушают
            console.warn('skipped sendCmd', { block: this.block, key: key, value: value });
            return;
        } 

        if (this.writeKeys.indexOf(key) == -1) {
            console.error('ignoring sendCmd', { block: this.block, key: key, value: value });
            return;
        }
        
        this._requestBus.send({ cmd: cmd, key: key, value: value });
    };

    Presenter.prototype.sendProps = function(changedProps) {
        if (!this.isInited) {
            //не спамим командами шину пока не уверены, что нас уже слушают
            console.warn('skipped sendProps', { block: this.block, props: changedProps });
            return;
        } 

        //молча отфильтровываем только допустимые свойства
        var blockProps = Objects.only(changedProps, this.writeKeys);

        if (Objects.isEmpty(blockProps)) {
            return;
        }
    
        this._requestBus.send({ cmd: 'set', value: blockProps });
    };

    /**
    * простейший случай для единственного свойства
    **/
    Presenter.prototype.sendProp = function(value) {
        var changedProps = {};
        changedProps[ this.writeKeys[0] ] = value;
        this.sendProps(changedProps);
    };

    /**
     * функция для самой первой отрисовки
     **/
    Presenter.prototype._initBlockProps = function(state) {
        var blockProps = Objects.only(state, this.blockKeys);
        this.onSetProps(blockProps, blockProps);
    };

    Presenter.prototype._setBlockProps = function(state, changedKeys) {
        var blockProps = Objects.only(state, this.blockKeys);
        this.onSetProps(blockProps, changedKeys);
    };

    Presenter.prototype._onResponse = function(response) {
        /*
         * начальная отрисовка должна произойти независимо от наличия каких-то изменений
         */
        if (!this.isInited) {
            this.isInited = true;
            this._initBlockProps(response.state);
            return;
        }

        /*
         * реагируем только на изменения наблюдаемых свойств
         */
        var changedKeys = Arrays.only(response.changedKeys, this.blockKeys);
        if (changedKeys.length) {
            this._setBlockProps(response.state, changedKeys);
            return;
        }
    };

    provide(Presenter);
}
);
