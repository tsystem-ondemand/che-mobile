modules.define('view-router', ['i-bem-dom'],
function(provide, bemDom, Base) {
provide(bemDom.declBlock(Base, {

    render: function(url) {
        console.log('render', '"' + url + '"');
        this.__base.apply(this, arguments);
    },

    redirect: function(url) {
        console.log('redirect', '"' + url + '"');
        this.__base.apply(this, arguments);
    }

}));
}
);
