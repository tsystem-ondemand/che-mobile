modules.define('message-bus',
function(provide, Base) {

    /**
     * @param instanceName необязательное имя конкретного экземпляра, нужно только для отладочных console.log() сообщений
     **/
    var MessageBus = function(instanceName) {
        Base.prototype.constructor.apply(this, arguments);
        this._name = instanceName;
    };
    MessageBus.prototype = Object.create(Base.prototype);
    MessageBus.prototype.constructor = MessageBus;

    MessageBus.prototype.send = function(message) {
        this._name && console.log(this._name + '.send', arguments);
        Base.prototype.send.apply(this, arguments);
    };

    MessageBus.prototype.subscribe = function(subscriber) {
        var subscriberKey = Base.prototype.subscribe.apply(this, arguments);
        this._name && console.log(this._name + '.subscribe', subscriberKey, arguments);
        return subscriberKey;
    };

    MessageBus.prototype.unsubscribe = function(subscriberKey) {
        this._name && console.log(this._name + '.unsubscribe', arguments);
        Base.prototype.unsubscribe.apply(this, arguments);
    };

    provide(MessageBus);

});
