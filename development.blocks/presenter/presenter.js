modules.define('presenter', ['objects'],
function(provide, Objects, Base) {

    var Presenter = function() {
        Base.prototype.constructor.apply(this, arguments);
    };
    Presenter.prototype = Object.create(Base.prototype);
    Presenter.prototype.constructor = Presenter;

    Presenter.prototype._initBlockProps = function(state) {
        var blockProps = Objects.only(state, this.blockKeys);
        console.log('onInitProps', { block: this.block, blockProps: blockProps });
        Base.prototype._initBlockProps.call(this, state);
    };

    Presenter.prototype._setBlockProps = function(state, changedKeys) {
        var blockProps = Objects.only(state, this.blockKeys);
        console.log('onSetProps', { block: this.block, blockProps: blockProps, changedKeys: changedKeys });
        Base.prototype._setBlockProps.call(this, state, changedKeys);
    };

    provide(Presenter);

}
);
